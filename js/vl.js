var lang = navigator.language || navigator.userLanguage;
var muted = false;
var groups = [];
var currentGroupIndex;
var currentPersonIndex;
//alert("de taal van je pc is: " + lang);

if (localStorage.getItem('groups') !== null) {
    groups = JSON.parse(localStorage.getItem('groups'));
}

var addGroupButton = document.getElementById("addGroupButton");
addGroupButton.addEventListener('click', createGroup);

var addMemberButton = document.getElementById("addMemberButton");
addMemberButton.addEventListener('click', function() {
    let container = document.getElementById('membersContainer');
    addMemberInput(container);
});

var addWishButton = document.getElementById('plus');
if (addWishButton !== null) {
    addWishButton.addEventListener('click', function(event) {
        let element = document.getElementById('listcont');
        createWishItem(element, "");
    });
}

var mute = document.getElementById("mte")
mute.addEventListener("click", function() {
    if (muted) {
        document.getElementById("mute").classList.remove("fas", "fa-volume-mute", "fa-2x");
        document.getElementById("mute").classList.add("fas", "fa-volume-up", "fa-2x");
        audioHandler();
    } else {
        document.getElementById("mute").classList.remove("fas", "fa-volume-up", "fa-2x");
        document.getElementById("mute").classList.add("fas", "fa-volume-mute", "fa-2x");
        audioHandler();
    }
});

function toGroup() {
    hideCard('home');
    document.getElementById('groupName').value = "";
    document.getElementById('cost').value = "";
    document.getElementById('membersContainer').innerHTML = "";
    showCard('addGroup');
}

function toEmail() {
    hideCard('home');
    showCard('emailContainer');
}

function toLottery() {
    hideCard('home');
    showCard('raffleEmailContainer');
}

function hideCard(id) {
    let element = document.getElementById(id);
    element.style.display = 'none';
}

function showCard(id) {
    let element = document.getElementById(id);
    element.style.display = 'block';
}

function addClickEvent() {
    var inpArr = document.getElementsByClassName("min");
    for (x of inpArr) {
        x.addEventListener("click", function(e) {
            e.target.parentElement.parentElement.parentElement.remove();
        });
    }
}

function Group(name, membersArray, maxCost) {
    this.name = name;
    this.maxCost = maxCost;
    this.members = membersArray;
}

function createGroup() {
    let groupName = document.getElementById("groupName");
    let cost = document.getElementById("cost");

    if (groupName != null || cost != null) {
        let group = new Group(groupName.value, [], cost.value);
        groups.push(group);
        currentGroupIndex = groups.indexOf(group);
        createMembers();
        saveList();
        hideCard('addGroup');
        showCard('home');
    }
}

function addMemberInput(parent) {
    if (parent != null) {
        let container = document.createElement("div");
        container.classList.add("memberInput");
        let email = document.createElement("input");
        email.placeholder = "e-mail";
        let name = document.createElement("input");
        name.placeholder = "naam";
        container.append(email);
        container.append(name);
        parent.append(container);
    }
}

function createMembers() {
    let memberElements = document.getElementsByClassName("memberInput");
    for (element of memberElements) {
        let email = element.childNodes[0];
        let name = element.childNodes[1];
        let member = {};
        member.email = email.value;
        member.name = name.value;
        member.raffled = false;
        member.list = [];
        member.raffled = false;
        member.groupName = groups[currentGroupIndex].name;
        groups[currentGroupIndex].members.push(member);
    }
}

function editPerson() {
    if ((document.getElementById("name").value.length != 0) && (document.getElementById("mail").value.length != 0) && (document.getElementById("mail").value.includes("@"))) {
        let listElements = document.getElementsByClassName("list");
        let list = [];
        for (element of listElements) {
            list.push(element.value);
        }

        let person = groups[currentGroupIndex].members[currentPersonIndex];
        person.name = document.getElementById("name").value;
        person.email = document.getElementById("mail").value;
        person.list = list;
        groups[currentGroupIndex].members[currentPersonIndex] = person;
        saveList();
        hideCard('wishlist');
        showCard('home');
    } else {
        alert("geen geldige input");
    }
}

function saveList() {
    localStorage.setItem("groups", JSON.stringify(groups));
}

function searchForPerson(id, callback) {
    if (localStorage.getItem("groups") != null) {
        let groupList = JSON.parse(localStorage.getItem("groups"));
        let emailadress = document.getElementById(id).value;
        let objectList = [];
        for (group of groupList) {
            let members = group.members;
            let chosen = members.filter(function(currentValue) {
                return filterOnMail(emailadress, currentValue);
            });
            if (chosen.length > 0) {
                for (result of chosen) {
                    objectList.push({
                        groupIndex: groupList.indexOf(group),
                        memberIndex: members.indexOf(result),
                        member: result
                    });
                }
            }
        }
        if (callback !== null) {
            callback(objectList);
        }
    }
}

function searchWishlist() {
    searchForPerson('emailInput', showWishList);
}

function searchForRaffle() {
    searchForPerson('raffleEmailInput', showRaffle);
}

function toWishlist(object) {
    hideCard('emailContainer');
    showPersonData(object);
}

function toRaffle(object) {
    hideCard('raffleEmailContainer');
    showRaffleData(object);
}

function showWishList(objectList) {
    if (objectList.length === 1) {
        toWishlist(objectList[0]);
    } else if (objectList.length > 1) {
        showGroupChoices('groupContainer', objectList, toWishlist);
    } else {
        emailIncorrect();
    }
}

function showRaffle(objectList) {
    if (objectList.length === 1) {
        toRaffle(objectList[0]);
    } else if (objectList.length > 1) {
        showGroupChoices('raffleGroupContainer', objectList, toRaffle);
    } else {
        emailIncorrect();
    }
}

function emailIncorrect() {
    document.getElementById('groupContainer').innerHTML = '';
    alert("E-mail not found in any group");
}

function showGroupChoices(id, choices, callback) {
    let element = document.getElementById(id);
    element.innerHTML = "<div>Meerdere groepen gevonden kies er een:</div>";
    for (let choice of choices) {
        let container = document.createElement('button');
        container.innerHTML = choice.member.groupName;
        container.classList.add("bttnwish", "groupItem", "fullwidth");
        element.append(container);
        container.onclick = function(e) {
            if (callback !== null) {
                callback(choice);
            }
        };
    }
}

function showPersonData(personData) {
    currentGroupIndex = personData.groupIndex;
    currentPersonIndex = personData.memberIndex;
    let data = personData.member;
    document.getElementById("name").value = data.name;
    document.getElementById("mail").value = data.email;
    showCard('wishlist');

    let listContainer = document.getElementById('listcont');
    listContainer.innerHTML = "";
    for (item of data.list) {
        createWishItem(listContainer, item);
    }
    addClickEvent();
}

function showRaffleData(object) {
    currentGroupIndex = object.groupIndex;
    currentPersonIndex = object.memberIndex;
    document.getElementById('raffleGreeting').innerHTML = "Welcome " + object.member.name + "! </br> Please raffle by pressing the button below.";
    document.getElementById('raffleList').innerHTML = "";
    document.getElementById('raffleResult').innerHTML = "";
    showCard('raffleContainer');
    document.getElementById('raffleButton').style.display = "initial";
    document.getElementById('doneButton').style.display = "none";
}

function createWishItem(parent, value) {
    if (parent != null) {
        let input = document.createElement('input');
        input.classList.add('list', 'width80');
        input.type = "text";
        input.placeholder = "Wish...";
        input.value = value;

        let min = document.createElement('div');
        min.id = 'min';

        min.classList.add('min');
        let span = document.createElement('span');
        span.style = 'color:crimson';

        let icon = document.createElement('i');
        icon.classList.add('far', 'fa-times-circle');
        span.append(icon);
        min.append(span);
        let inputDiv = document.createElement('div');
        inputDiv.append(input);
        inputDiv.append(min);
        parent.append(inputDiv);
        inputDiv.style.animation = "fadeIn 1s 1";
        addClickEvent();
    } else {
        console.error("Can't create wish item, no parent object found");
    }
}

function filterOnMail(emailadress, currentValue) {
    return (currentValue.email === emailadress);
}

function pickRaffle() {
    let searchName = groups[currentGroupIndex].members[currentPersonIndex].email;
    var randomNamePicks = [];
    var checkArray = [];

    for (let x of groups[currentGroupIndex].members) {
        checkArray.push(x.email);
    }
    if (checkArray.includes(searchName)) {
        for (let y of groups[currentGroupIndex].members) {
            if ((y.email !== searchName) && (!y.raffled)) {
                randomNamePicks.push(y);
            }
        }
        if (randomNamePicks.length > 0) {
            let index = Math.floor(Math.random() * randomNamePicks.length)
            let firstPick = randomNamePicks[index].name + " (" + randomNamePicks[index].email + ")";
            document.getElementById("raffleResult").innerHTML += "<br>You have raffled: " + firstPick;
            randomNamePicks[index].raffled = true;
            saveList();
            let listContainer = document.getElementById('raffleList');
            let wishlistElement = document.createElement('h2');
            wishlistElement.innerHTML = "Wishlist:";
            listContainer.append(wishlistElement);
            for (let item of randomNamePicks[index].list) {
                let itemElement = document.createElement('div');
                itemElement.innerHTML = item;
                listContainer.append(itemElement);
            }
        } else {
            alert("No more raffles to pick!");
        }
        document.getElementById('raffleButton').style.display = "none";
        document.getElementById('doneButton').style.display = "block";
    }
}

function onRaffleDone() {
    hideCard('raffleContainer');
    showCard('home');
    currentGroupIndex = '';
    currentPersonIndex = '';
}

function audioHandler() {
    if (muted) {
        document.getElementById("iframeAudio").muted = false;
        muted = false;
    } else {
        document.getElementById("iframeAudio").muted = true;
        muted = true;
    }
}